// orat.io integration
module.exports = (Franz) => {
  function getMessages() {
    let direct = 0
    direct = jQuery("span[title*='Inbox'] + div > span").first().text().replace(" Unread", "");
    Franz.setBadge(direct);
  }

  Franz.loop(getMessages);
}
